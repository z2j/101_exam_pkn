import random

listaA = ["papier", "kamień", "nożyce"]

print("Witamy w grze papier-kamień-nożyce!\n")

choice = ""

while choice != "x":
    choice = str(input("Co wybierasz:\n"
                       "a - symulacja\n"
                       "b - gra z komputerem\n"
                       "x - wyjście\n"
                       "Twój wybór: ")).lower()

    if choice == "a":
        print("\n\nWybrał_ś symulację\n")

        wyborA = random.choice(listaA)
        wyborB = random.choice(listaA)
        print(f"Gracz A: {wyborA}\n"
              f"Gracz B: {wyborB}\n")

        if (wyborA == listaA[0] and wyborB == listaA[0]) or \
        (wyborA == listaA[1] and wyborB == listaA[1]) or \
        (wyborA == listaA[2] and wyborB == listaA[2]):
            print("Remis!\n")

        elif (wyborA == listaA[0] and wyborB == listaA[1]) or \
                (wyborA == listaA[1] and wyborB == listaA[2]) or \
                (wyborA == listaA[2] and wyborB == listaA[0]):
            print("Wygyrwa Gracz A\n")

        else:
            print("Wygrywa Gracz B\n")

    elif choice == "b":
        print("\nWybrał_ś grę z komputerem\n")
        name = input("Podaj swoje imię: ")

        try:
            wyborA = listaA[int(input("Co wybierasz: 0 - papier, 1 - kamień, 2 - nożyce: "))]

        except IndexError:
            print("\nWidzę, że emocje sięgają zenitu. Wybrał_ś nieprawidłową wartość.\n"
                  "Spróbuj jeszcze raz :)\n")

        else:
            if wyborA == 0 or 1 or 2:
                wyborB = random.choice(listaA)
                print(f"\n{name}: {wyborA}\n"
                      f"Komputer: {wyborB}\n")

                if (wyborA == listaA[0] and wyborB == listaA[0]) or \
                        (wyborA == listaA[1] and wyborB == listaA[1]) or \
                        (wyborA == listaA[2] and wyborB == listaA[2]):
                    print("Remis!\n")

                elif (wyborA == listaA[0] and wyborB == listaA[1]) or \
                        (wyborA == listaA[1] and wyborB == listaA[2]) or \
                        (wyborA == listaA[2] and wyborB == listaA[0]):
                    print(f"Wygyrwa {name}\n")

                else:
                    print("Wygrywa Komputer, przykro mi\n")


    elif choice == "x":
        print("\nDo zobaczenia!\n")
        break

    else:
        print("Po co te nerwy! Spokojnie wybierz opcję z menu :)\n")